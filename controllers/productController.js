const Product = require("../models/Product");

module.exports.addProduct = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newProduct" and instantiates a new "product" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price,
			isActive: data.product.isActive,
			createdOn:data.product.createdOn,
			orders: data.product.orders
		});
		// Saves the created object to our database
		return newProduct.save().then((product, error) => {
		
			if (error) {
			
				return ("Error! Product not added successfully!");
			}
			else {
				
				return ("Product is successfully added.");
			}
		})
	}
	else {
		
		return ("Error! User is not and admin!");
	}
}



// Retrieve ALL product
module.exports.getAllProduct = () =>{
	return Product.find({}).then(result =>{
		return result;
	if (error) {
		
			return ("Error! Cannot get all products!");
		}
		else {
			
			return result;
		}
	})
};

// Retrieve ACTIVE product
module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then(result => {
		return result;
	})
};


// Retrieve specific product
module.exports.getSpecificProduct = (reqParams)=>{
	return Product.findById(reqParams.productId).then(result => {
		return result;
if (error) {
			
			return ("Error! Cannot get specific product!");
		}
		else {
		
			return result;
		}
	})
};

// Update a Product

module.exports.updateProduct = (reqParams, reqBody) => {
	let updateProduct = {
 		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price,
 		isActive: reqBody.isActive,
 		createdOn: reqBody.createdOn,
 		
 	};

 	return Product.findByIdAndUpdate(reqParams.productId,
 		updateProduct).then((product, error) => {
 			if(error){
 				return ("Error! Cannot update product information!");
 			} else {
 				return ("Product information has been successfully updated.")
 			};
 		});
	};




	//archive product

module.exports.archiveProduct = (reqParams) => {
	let updateActiveField = {
		isActive: false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return ("Error! Cannot update product availability!");
		} else {
			return "Product was successfully archived.";
		};
	});
};