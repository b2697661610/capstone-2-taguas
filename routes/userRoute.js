const express = require("express");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

// Route for checking if the user's email already exists in the database
 const userController = require("../controllers/userController");

const auth = require("../auth");



// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details

// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can order to a product
router.post("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

// Provides the user's ID for the getProfile controller method
userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});



router.post ("/orders", auth.verify,(req, res) =>{
	let data ={
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	userController.userOrders(data).then(resultFromController =>
		res.send(resultFromController))
});


router.get('/:userId/orders', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	if (data) {
		userController.userOrders(data).then(resultFromController => res.send(resultFromController));
	}
	else {
	
		res.send("Error! User not authenticated!")
	}
});

router.post('/checkout', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	if (data) {
		userController.userCheckout(data).then(resultFromControlle => res.send(resultFromController));
	}
	else {

		res.send("Error! User not authenticated!")
	}
})
router.put("/:userId/admin", auth.verify, (req,res) => {

	const adminData = auth.decode(req.headers.authorization);

	userController.setAsAdmin(req.params, adminData).then(resultFromController => res.send(resultFromController));
})
router.get("/all", auth.verify, (req, res) => {

	const adminData = auth.decode(req.headers.authorization);

	userController.getAllUsers(adminData).then(resultFromController => res.send(resultFromController));
})



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
